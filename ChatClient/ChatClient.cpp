#include "stdafx.h"
#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <thread>

using boost::asio::ip::tcp;

class ChatClient
{
public:
	ChatClient(boost::asio::io_service& io_service,
		tcp::resolver::iterator endpoint_iterator)
		: io_service_(io_service),
		socket_(io_service)
	{
		Connect(endpoint_iterator);
	}

	void ReceiveData()
	{
		boost::asio::async_read(socket_, boost::asio::buffer(data_in, sizeof(data_in)),
			[this](boost::system::error_code ec, size_t length)
		{
			if (ec)
			{
				std::cout << ec.message() << std::endl;
				std::cout << "bytes transferred " << length << std::endl;
				socket_.close();
			}
			else
			{
				std::cout << data_in << std::endl;
				ReceiveData();
			}
		});
	}

	void DoWrite()
	{
		std::string the_message;

		do
		{
			std::getline(std::cin, the_message);

			if (the_message.length() == 0)
			{
				std::cout << "Message too short" << std::endl;
			}
			else if (the_message.length() > 511)
			{
				std::cout << "Message too long" << std::endl;
			}
			else
			{
				strcpy_s(data_out, the_message.c_str());
			}

		} 
		while (strlen(data_out) == 0 || strlen(data_out) > 512);



		try
		{
			boost::asio::async_write(socket_, boost::asio::buffer(data_out,	sizeof(data_out)),
				[this](boost::system::error_code ec, std::size_t length)
			{
				if (ec)
				{
					std::cout << ec.message() << std::endl;
					std::cout << "bytes transferred " << length << std::endl;
					socket_.close();
				}
			});
		}
		catch (std::exception e)
		{
			std::cerr << e.what() << std::endl;
		}

		DoWrite();
	}



private:

	void Connect(tcp::resolver::iterator endpoint_iterator)
	{
		boost::asio::async_connect(socket_, endpoint_iterator,
			[this](boost::system::error_code ec, tcp::resolver::iterator)
		{
			if (!ec)
			{
				std::cout << "connected" << std::endl;
			}
		});
	}

	char data_in[512];
	char data_out[512];
	boost::asio::io_service& io_service_;
	tcp::socket socket_;

};




int main(int argc, char* argv[])
{
	try
	{
		if (argc != 3)
		{
			std::cerr << "Usage: chat_client <host> <port>\n";
			return 1;
		}

		boost::asio::io_service io_service;
		tcp::resolver resolver(io_service);
		tcp::socket socket(io_service);

		auto endpoint_iterator = resolver.resolve({ argv[1], argv[2] });

		ChatClient c(io_service, endpoint_iterator);

		std::thread t([&io_service]() { io_service.run(); });

		c.ReceiveData();
		c.DoWrite();

		for (;;)
		{

		}

		t.join();

	}
	catch (std::exception &e)
	{
		std::cerr << "Exception " << e.what() << std::endl;
	}

	return 0;
}

